# --------------------------
# Common/General parameters
# --------------------------
variable "ecs_asg_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}
variable "ecs_asg_module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default = null
}


variable "ecs_asg_name_prefix" {
  # Conflicts with `name`.
  description = "(Required) Creates a unique name beginning with the specified prefix. NOTE: must up to 16 characters long!"
  type        = string
}
variable "ecs_asg_tags" {
  description = "A mapping of common tags to assign to the resources."
  type        = map(string)
  default     = {}
}
variable "ecs_asg_instance_tags" {
  description = "A mapping of tags to assign only to EC2 instances."
  type        = map(string)
  default     = {}
}
variable "ecs_asg_volume_tags" {
  description = "A mapping of tags to assign only to EBS volumes."
  type        = map(string)
  default     = {}
}


# --------------------------------
# Network/Security/IAM parameters
# --------------------------------
variable "ecs_asg_vpc_id" {
  description = "(Required) The VPC ID where launced EC2 will reside."
  type        = string
}
variable "ecs_asg_open_ports_list" {
  description = "List of TCP ports will be opened on the EC2 instances within AutoScaling Group."
  type = list(object({
    port     = number
    protocol = string
  }))
  default = []
}
variable "ecs_asg_allowed_cidr_blocks_list" {
  description = "List of CIDR blocks allowed to access the EC2 instances within AutoScaling Group."
  type        = list(string)
  default     = []
}


# -------------------------
# Launch Template settings
# -------------------------
variable "ecs_asg_block_device_mappings" {
  description = "Specify volumes to attach to the instance besides the volumes specified by the AMI."
  # To find more info, look at:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/block-device-mapping-concepts.html
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#block-devices
  type = list(object({
    device_name  = string
    no_device    = bool
    virtual_name = string
    ebs = object({
      delete_on_termination = bool
      encrypted             = bool
      iops                  = number
      kms_key_id            = string
      snapshot_id           = string
      volume_size           = number
      volume_type           = string
    })
  }))
  default = []
}

variable "ecs_asg_credit_specification" {
  description = "Customize the credit specification of the instances."
  # See:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#credit-specification
  type = object({
    cpu_credits = string
  })
  default = null
}

variable "ecs_asg_disable_api_termination" {
  description = "If `true`, enables EC2 Instance Termination Protection."
  # See:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/terminating-instances.html#Using_ChangingDisableAPITermination
  type    = bool
  default = false
}

variable "ecs_asg_elastic_gpu_specifications" {
  description = "The elastic GPU to attach to the instance."
  # See specification of Elastic GPU for more details at:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#elastic-gpu
  type = object({
    type = string
  })
  default = null
}

variable "ecs_asg_image_id" {
  description = "The AMI from which to launch the instance."
  type        = string
  default     = ""
}

variable "ecs_asg_instance_initiated_shutdown_behavior" {
  description = "Shutdown behavior for the instance."
  # Can be:
  #   * stop
  #   * terminate
  type    = string
  default = "terminate"
}

variable "ecs_asg_instance_market_options" {
  description = "The market (purchasing) option for the instances."
  # See for details:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#market-options
  type = object({
    market_type = string
    spot_options = object({
      block_duration_minutes         = number
      instance_interruption_behavior = string
      max_price                      = number
      spot_instance_type             = string
      valid_until                    = string
    })
  })
  default = null
}

variable "ecs_asg_instance_type" {
  description = "(Required) The type of the instance."
  type        = string
}

variable "ecs_asg_key_name" {
  description = "The SSH key name that should be used for the instance."
  type        = string
  default     = ""
}

variable "ecs_asg_placement" {
  description = "The placement specifications of the instances."
  # See specification of Placement block at:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#placement-1
  type = object({
    affinity          = string
    availability_zone = string
    group_name        = string
    host_id           = string
    tenancy           = string
  })
  default = null
}

variable "ecs_asg_user_data" {
  description = "The Base64-encoded user data to provide when launching the instances."
  type        = string
  default     = ""
}

variable "ecs_asg_instance_iam_policy" {
  description = "The IAM policy as JSON formatted string to include into instance profile."
  type        = string
  default     = ""
}

variable "ecs_asg_iam_instance_profile_name" {
  description = "The IAM instance profile name to associate with launched instances."
  type        = string
  default     = ""
}

variable "ecs_asg_enable_detailed_monitoring" {
  description = "Enable/disable detailed monitoring."
  type        = bool
  default     = true
}

variable "ecs_asg_associate_public_ip_address" {
  description = "Associate a public IP address with an instance in a VPC."
  type        = bool
  default     = false
}

variable "ecs_asg_security_group_ids" {
  description = "A list of associated security group IDs."
  type        = list(string)
  default     = []
}


# ---------------------------
# AutoScaling Group settings
# ---------------------------
variable "ecs_asg_force_replacement" {
  description = "Whether to replace EC2 instances and existing ASG with new version if any of parameters changes."
  type        = bool
  default     = false
}

variable "ecs_asg_max_size" {
  description = "(Required) The maximum size of the autoscale group."
  type        = number
}

variable "ecs_asg_min_size" {
  description = "(Required) The minimum size of the autoscale group."
  type        = number
}

variable "ecs_asg_default_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes before another scaling activity can start."
  type        = number
  default     = 180
}

variable "ecs_asg_health_check_grace_period" {
  description = "Time (in seconds) after instance comes into service before checking health."
  type        = number
  default     = 150
}

variable "ecs_asg_health_check_type" {
  description = "Controls how health checking is done. Valid values are `EC2` or `ELB`."
  type        = string
  default     = "EC2"
}

variable "ecs_asg_force_delete" {
  # You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally,
  # Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially
  # leaves resources dangling
  description = "Allows deleting the autoscaling group without waiting for all instances in the pool to terminate."
  type        = bool
  default     = false
}

variable "ecs_asg_load_balancers" {
  # Only valid for classic load balancers. For ALBs, use `target_group_arns` instead
  description = "A list of elastic load balancer names to add to the autoscaling group names."
  type        = list(string)
  default     = []
}
variable "ecs_asg_target_group_arns" {
  description = "A list of `aws_alb_target_group` ARNs, for use with Application Load Balancing."
  type        = list(string)
  default     = []
}

variable "ecs_asg_subnet_ids" {
  description = "(Required) A list of subnet IDs to launch resources in."
  type        = list(string)
}

variable "ecs_asg_termination_policies" {
  description = "A list of policies to decide how the instances in the auto scale group should be terminated."
  type        = list(string)
  # The allowed values are:
  #   * OldestInstance
  #   * NewestInstance
  #   * OldestLaunchConfiguration
  #   * ClosestToNextInstanceHour
  #   * OldestLaunchTemplate
  #   * AllocationStrategy
  #   * Default
  default = ["Default"]
}

variable "ecs_asg_suspended_processes" {
  description = "A list of processes to suspend for the AutoScaling Group."
  type        = list(string)
  # The allowed values are:
  #   * Launch
  #   * Terminate
  #   * HealthCheck
  #   * ReplaceUnhealthy
  #   * AZRebalance
  #   * AlarmNotification
  #   * ScheduledActions
  #   * AddToLoadBalancer
  # Note that if you suspend either the `Launch` or `Terminate` process types, it can prevent your autoscaling
  # group from functioning properly.
  default = []
}

variable "ecs_asg_placement_group" {
  description = "The name of the placement group into which you'll launch your instances, if any."
  type        = string
  default     = ""
}

variable "ecs_asg_enabled_metrics" {
  description = "A list of metrics to collect."
  type        = list(string)
  # The allowed values are:
  #   * GroupMinSize,
  #   * GroupMaxSize,
  #   * GroupDesiredCapacity,
  #   * GroupInServiceInstances,
  #   * GroupPendingInstances,
  #   * GroupStandbyInstances,
  #   * GroupTerminatingInstances,
  #   * GroupTotalInstances.
  default = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}

variable "ecs_asg_wait_for_capacity_timeout" {
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out."
  type        = string
  # Setting this to '0' causes Terraform to skip all Capacity Waiting behavior
  default = "10m"
}

variable "ecs_asg_min_elb_capacity" {
  description = "Setting this causes Terraform to wait for this number of instances to show up healthy in the ELB only on creation. Updates will not wait on ELB instance number changes."
  type        = number
  default     = 0
}

variable "ecs_asg_wait_for_elb_capacity" {
  # Takes precedence over `min_elb_capacity` behavior.
  description = "Setting this will cause Terraform to wait for exactly this number of healthy instances in all attached load balancers on both create and update operations."
  type        = number
  default     = 0
}

variable "ecs_asg_protect_from_scale_in" {
  description = "Allows setting instance protection."
  # The autoscaling group will not select instances with this setting for terminination during scale in events
  type    = bool
  default = false
}

variable "ecs_asg_service_linked_role_arn" {
  description = "The ARN of the service-linked role that the ASG will use to call other AWS services."
  type        = string
  default     = ""
}


# ---------------------
# Autoscaling policies
# ---------------------
variable "ecs_asg_autoscaling_policies_enabled" {
  description = "Whether to create `aws_autoscaling_policy` and `aws_cloudwatch_metric_alarm` resources to control Auto Scaling."
  type        = bool
  default     = true
}

### UP ###
variable "ecs_asg_scale_up_adjustment_type" {
  description = "Specifies whether the adjustment is an absolute number or a percentage of the current capacity."
  type        = string
  # Valid values are:
  #   * ChangeInCapacity
  #   * ExactCapacity
  #   * PercentChangeInCapacity
  default = "ChangeInCapacity"
}
variable "ecs_asg_scale_up_policy_type" {
  description = "The scalling policy type, either 'SimpleScaling', 'StepScaling' or 'TargetTrackingScaling'."
  type        = string
  default     = "SimpleScaling"
}
variable "ecs_asg_scale_up_cooldown_seconds" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start."
  type        = number
  default     = 180
}
variable "ecs_asg_scale_up_scaling_adjustment" {
  # Adjustment type determines the interpretation of this number (e.g. as an absolute number or as a percentage
  # of the existing Auto Scaling group size). A positive increment adds to the current capacity and a negative
  # value removes from the current capacity
  description = "The number of instances by which to scale."
  default     = 1
}

### DOWN ###
variable "ecs_asg_scale_down_adjustment_type" {
  description = "Specifies whether the adjustment is an absolute number or a percentage of the current capacity."
  type        = string
  # Valid values are:
  #   * ChangeInCapacity
  #   * ExactCapacity
  #   * PercentChangeInCapacity
  default = "ChangeInCapacity"
}
variable "ecs_asg_scale_down_policy_type" {
  description = "The scalling policy type, either 'SimpleScaling', 'StepScaling' or 'TargetTrackingScaling'."
  type        = string
  default     = "SimpleScaling"
}
variable "ecs_asg_scale_down_cooldown_seconds" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start."
  type        = number
  default     = 180
}
variable "ecs_asg_scale_down_scaling_adjustment" {
  # Adjustment type determines the interpretation of this number (e.g. as an absolute number or as a percentage
  # of the existing Auto Scaling group size). A positive increment adds to the current capacity and a negative
  # value removes from the current capacity
  description = "The number of instances by which to scale."
  default     = -1
}


# ----------------------------------
# CloudWatch Metric Alarms settings
# ----------------------------------
### HIGH ###
variable "ecs_asg_cpu_utilization_high_evaluation_periods" {
  description = "The number of periods over which data is compared to the specified threshold."
  type        = number
  default     = 2
}
variable "ecs_asg_cpu_utilization_high_period_seconds" {
  description = "The period in seconds over which the specified statistic is applied."
  type        = number
  default     = 180
}
variable "ecs_asg_cpu_utilization_high_statistic" {
  description = "The statistic to apply to the alarm's associated metric."
  type        = string
  #  Either of the following is supported:
  #   * SampleCount
  #   * Average
  #   * Sum
  #   * Minimum
  #   * Maximum
  default = "Average"
}
variable "ecs_asg_cpu_utilization_high_threshold_percent" {
  description = "The value against which the specified statistic is compared."
  type        = number
  default     = 75
}

### LOW ###
variable "ecs_asg_cpu_utilization_low_evaluation_periods" {
  description = "The number of periods over which data is compared to the specified threshold."
  type        = number
  default     = 2
}
variable "ecs_asg_cpu_utilization_low_period_seconds" {
  description = "The period in seconds over which the specified statistic is applied."
  type        = number
  default     = 180
}
variable "ecs_asg_cpu_utilization_low_statistic" {
  description = "The statistic to apply to the alarm's associated metric."
  type        = string
  #  Either of the following is supported:
  #   * SampleCount
  #   * Average
  #   * Sum
  #   * Minimum
  #   * Maximum
  default = "Average"
}
variable "ecs_asg_cpu_utilization_low_threshold_percent" {
  description = "The value against which the specified statistic is compared."
  type        = number
  default     = 30
}
