# --------------------------
# Common/General parameters
# --------------------------
# Whether to create the resources (`false` prevents the module from creating any resources).
ecs_asg_module_enabled = true
# Emulation of `depends_on` behavior for the module (non zero length string can be used to have current module
# wait for the specified resource.)
ecs_asg_module_depends_on = null
# (Required) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
ecs_asg_name_prefix = "test3"
# A mapping of common tags to assign to the resources.
ecs_asg_tags = {
  common_tag1 = "value1"
}
# A mapping of tags to assign only to EC2 instances.
ecs_asg_instance_tags = {
  ec2_tag1 = "instance_value"
}
# A mapping of tags to assign only to EBS volumes.
ecs_asg_volume_tags = {
  ebs_tag1 = "volume_value"
}

# --------------------------------
# Network/Security/IAM parameters
# --------------------------------
# (Required) The VPC ID where launced EC2 will reside.
ecs_asg_vpc_id = "vpc-test3"
# List of TCP ports will be opened on the EC2 instances within AutoScaling Group.
ecs_asg_open_ports_list = []
# List of CIDR blocks allowed to access the EC2 instances within AutoScaling Group.
ecs_asg_allowed_cidr_blocks_list = []

# -------------------------
# Launch Template settings
# -------------------------
# Specify volumes to attach to the instance besides the volumes specified by the AMI.
  # To find more info, look at:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/block-device-mapping-concepts.html
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#block-devices
ecs_asg_block_device_mappings = []
# Customize the credit specification of the instances. See:
#   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#credit-specification
ecs_asg_credit_specification = null
# If `true`, enables EC2 Instance Termination Protection. See:
#   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/terminating-instances.html#Using_ChangingDisableAPITermination
ecs_asg_disable_api_termination = false
# The elastic GPU to attach to the instance.
# See specification of Elastic GPU for more details at:
#   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#elastic-gpu
ecs_asg_elastic_gpu_specifications = null
# The AMI from which to launch the instance.
ecs_asg_image_id = ""
# Shutdown behavior for the instance. Can be:
#   * stop
#   * terminate
ecs_asg_instance_initiated_shutdown_behavior = "terminate"
# The market (purchasing) option for the instances. See for details:
#   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#market-options
ecs_asg_instance_market_options = null
# (Required) The type of the instance.
ecs_asg_instance_type = "t3.micro"
# The SSH key name that should be used for the instance.
ecs_asg_key_name = ""
# The placement specifications of the instances. See specification of Placement block at:
#   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#placement-1
ecs_asg_placement = null
# The Base64-encoded user data to provide when launching the instances.
ecs_asg_user_data = ""
# The IAM policy as JSON formatted string to include into instance profile.
ecs_asg_instance_iam_policy = ""
# The IAM instance profile name to associate with launched instances.
ecs_asg_iam_instance_profile_name = ""
# Enable/disable detailed monitoring.
ecs_asg_enable_detailed_monitoring = true
# Associate a public IP address with an instance in a VPC.
ecs_asg_associate_public_ip_address = false
# A list of associated security group IDs.
ecs_asg_security_group_ids = []

# ---------------------------
# AutoScaling Group settings
# ---------------------------
# Whether to replace EC2 instances and existing ASG with new version if any of parameters changes.
ecs_asg_force_replacement = false
# (Required) The maximum size of the autoscale group.
ecs_asg_max_size = 1
# (Required) The minimum size of the autoscale group.
ecs_asg_min_size = 1
# The amount of time, in seconds, after a scaling activity completes before another scaling activity can start.
ecs_asg_default_cooldown = 180
# Time (in seconds) after instance comes into service before checking health.
ecs_asg_health_check_grace_period = 150
# Controls how health checking is done. Valid values are `EC2` or `ELB`.
ecs_asg_health_check_type = "EC2"
# Allows deleting the autoscaling group without waiting for all instances in the pool to terminate.
# You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally,
# Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially
# leaves resources dangling
ecs_asg_force_delete = false
# A list of elastic load balancer names to add to the autoscaling group names.
# NOTE(!): only valid for classic load balancers. For ALBs, use `target_group_arns` instead
ecs_asg_load_balancers = []
# A list of `aws_alb_target_group` ARNs, for use with Application Load Balancing.
ecs_asg_target_group_arns = []
# (Required) A list of subnet IDs to launch resources in.
ecs_asg_subnet_ids = [
  "subnet-abcd1234"
]
# A list of policies to decide how the instances in the auto scale group should be terminated.
ecs_asg_termination_policies = ["Default"]
# A list of processes to suspend for the AutoScaling Group.
ecs_asg_suspended_processes = []
# The name of the placement group into which you'll launch your instances, if any.
ecs_asg_placement_group = ""
# A list of metrics to collect.
ecs_asg_enabled_metrics = [
  "GroupMinSize",
  "GroupMaxSize",
  "GroupDesiredCapacity",
  "GroupInServiceInstances",
  "GroupPendingInstances",
  "GroupStandbyInstances",
  "GroupTerminatingInstances",
  "GroupTotalInstances",
]
# A maximum duration that Terraform should wait for ASG instances to be healthy before timing out.
# NOTE(!): setting this to '0' causes Terraform to skip all Capacity Waiting behavior
ecs_asg_wait_for_capacity_timeout = "10m"
# Setting this causes Terraform to wait for this number of instances to show up healthy in the ELB only on creation.
# Updates will not wait on ELB instance number changes.
ecs_asg_min_elb_capacity = 0
# Setting this will cause Terraform to wait for exactly this number of healthy instances in all attached load
# balancers on both create and update operations. Takes precedence over `min_elb_capacity` behavior!
ecs_asg_wait_for_elb_capacity = 0
# Allows setting instance protection. The autoscaling group will not select instances with this setting for
# terminination during scale in events.
ecs_asg_protect_from_scale_in = false
# The ARN of the service-linked role that the ASG will use to call other AWS services.
ecs_asg_service_linked_role_arn = ""

# ---------------------
# Autoscaling policies
# ---------------------
# Whether to create `aws_autoscaling_policy` and `aws_cloudwatch_metric_alarm` resources to control Auto Scaling.
ecs_asg_autoscaling_policies_enabled = true
### UP ###
# Specifies whether the adjustment is an absolute number or a percentage of the current capacity. Valid values are:
#   * ChangeInCapacity
#   * ExactCapacity
#   * PercentChangeInCapacity
ecs_asg_scale_up_adjustment_type = "ChangeInCapacity"
# The scalling policy type, either 'SimpleScaling', 'StepScaling' or 'TargetTrackingScaling'.
ecs_asg_scale_up_policy_type = "SimpleScaling"
# The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start.
ecs_asg_scale_up_cooldown_seconds = 180
# The number of instances by which to scale. Adjustment type determines the interpretation of this number
# (e.g. as an absolute number or as a percentage of the existing Auto Scaling group size). A positive increment
# adds to the current capacity and a negative value removes from the current capacity
ecs_asg_scale_up_scaling_adjustment = 1
### DOWN ###
# Specifies whether the adjustment is an absolute number or a percentage of the current capacity. Valid values are:
#   * ChangeInCapacity
#   * ExactCapacity
#   * PercentChangeInCapacity
ecs_asg_scale_down_adjustment_type = "ChangeInCapacity"
# The scalling policy type, either 'SimpleScaling', 'StepScaling' or 'TargetTrackingScaling'.
ecs_asg_scale_down_policy_type = "SimpleScaling"
# The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start.
ecs_asg_scale_down_cooldown_seconds = 180
# The number of instances by which to scale. Adjustment type determines the interpretation of this number
# (e.g. as an absolute number or as a percentage of the existing Auto Scaling group size). A positive increment
# adds to the current capacity and a negative value removes from the current capacity
ecs_asg_scale_down_scaling_adjustment = -1

# ----------------------------------
# CloudWatch Metric Alarms settings
# ----------------------------------
### HIGH ###
# The number of periods over which data is compared to the specified threshold.
ecs_asg_cpu_utilization_high_evaluation_periods = 2
# The period in seconds over which the specified statistic is applied.
ecs_asg_cpu_utilization_high_period_seconds = 180
# The statistic to apply to the alarm's associated metric. Either of the following is supported:
#   * SampleCount
#   * Average
#   * Sum
#   * Minimum
#   * Maximum
ecs_asg_cpu_utilization_high_statistic = "Average"
# The value against which the specified statistic is compared.
ecs_asg_cpu_utilization_high_threshold_percent = 75

### LOW ###
# The number of periods over which data is compared to the specified threshold.
ecs_asg_cpu_utilization_low_evaluation_periods = 2
# The period in seconds over which the specified statistic is applied.
ecs_asg_cpu_utilization_low_period_seconds = 120
# The statistic to apply to the alarm's associated metric. Either of the following is supported:
#   * SampleCount
#   * Average
#   * Sum
#   * Minimum
#   * Maximum
ecs_asg_cpu_utilization_low_statistic = "Average"
# The value against which the specified statistic is compared.
ecs_asg_cpu_utilization_low_threshold_percent = 30
