locals {
  enabled = var.ecs_asg_module_enabled ? true : false

  common_tags = merge(
    var.ecs_asg_tags,
    {
      terraform = true
    }
  )

  # if prefix ends with dash '-' leave it untouched, or attach dash at the end of string otherwise
  asg_name_prefix         = substr(var.ecs_asg_name_prefix, (length(var.ecs_asg_name_prefix) - 1), 1) == "-" ? var.ecs_asg_name_prefix : format("%s-", var.ecs_asg_name_prefix)
  asg_name_prefix_wo_dash = substr(local.asg_name_prefix, 0, length(local.asg_name_prefix) - 1)
}


# ----------------------------------------------------------------------------------------
# Create IAM instance profile for EC2 instances which ECS Service is running ECS Tasks on
# ----------------------------------------------------------------------------------------
data "aws_iam_policy_document" "ec2_assume_role_policy" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid = "AllowEC2InstancesToAssumeThisRole"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
    effect = "Allow"
  }
}
resource "aws_iam_role" "ec2_instance" {
  # Provides an IAM role.

  # NOTE: If policies are attached to the role via the `aws_iam_policy_attachment` resource and you are modifying the
  # role `name` or `path`, the `force_detach_policies` argument must be set to `true` and applied before attempting the
  # operation otherwise you will encounter a `DeleteConflict` error.
  # The `aws_iam_role_policy_attachment` resource (recommended) does not have this requirement.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-role-", local.asg_name_prefix_wo_dash)
  # (Optional) The description of the role.
  description = "The IAM role for EC2 Instances launched by LaunchTemplate within the AutoScaling [${local.asg_name_prefix_wo_dash}] Group."
  # Key-value mapping of tags for the IAM role.
  tags = merge(
    local.common_tags,
    {
      Name = format("%s-role", local.asg_name_prefix_wo_dash),
    },
  )

  # (Optional) The path to the role. See IAM Identifiers for more information:
  #   * https://docs.aws.amazon.com/IAM/latest/UserGuide/Using_Identifiers.html
  path = "/"
  # (Required) The policy that grants an entity permission to assume the role.
  assume_role_policy = join("", data.aws_iam_policy_document.ec2_assume_role_policy.*.json)
}

resource "aws_iam_role_policy_attachment" "ecs_ec2" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.ec2_instance.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}
resource "aws_iam_role_policy_attachment" "cw_agent" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.ec2_instance.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}
resource "aws_iam_role_policy_attachment" "cw_logs_ro" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.ec2_instance.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsReadOnlyAccess"
}
resource "aws_iam_role_policy_attachment" "ssm_core" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.ec2_instance.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy_document" "ecs_instance" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid = "GiveEC2InstancesMorePermissions"
    actions = [
      "ec2:Describe*",
      "ecs:Describe*",
      "ecs:List*",
      "ecs:Poll*",
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "empty" {
  count = (local.enabled && var.ecs_asg_instance_iam_policy == "") ? 1 : 0
}

data "aws_iam_policy_document" "ec2_instance" {
  count = local.enabled ? 1 : 0

  # (Optional) - An IAM policy document to import as a base for the current policy document. Statements with
  # non-blank sids in the current policy document will overwrite statements with the same sid in the source json.
  # Statements without an sid cannot be overwritten.
  source_json = var.ecs_asg_instance_iam_policy == "" ? join("", data.aws_iam_policy_document.empty.*.json) : var.ecs_asg_instance_iam_policy
  # (Optional) - An IAM policy document to import and override the current policy document. Statements with non-blank
  # sids in the override document will overwrite statements with the same sid in the current document. Statements
  # without an sid cannot be overwritten.
  override_json = join("", data.aws_iam_policy_document.ecs_instance.*.json)
}

resource "aws_iam_role_policy" "ec2_instance" {
  # Provides an IAM role policy.
  count = local.enabled ? 1 : 0

  # (Optional) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-policy-", local.asg_name_prefix_wo_dash)
  # (Required) The policy document. This is a JSON formatted string.
  policy = join("", data.aws_iam_policy_document.ec2_instance.*.json)
  # (Required) The IAM role to attach to the policy.
  role = join("", aws_iam_role.ec2_instance.*.id)
}

resource "aws_iam_instance_profile" "ec2" {
  # Provides an IAM instance profile.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with name.
  name_prefix = format("asg-%s-instance-profile-", local.asg_name_prefix_wo_dash)
  # (Optional) The role name to include in the profile.
  role = join("", aws_iam_role.ec2_instance.*.name)
}


# -----------------------------------------
# Create Security Groups for EC2 instances
# -----------------------------------------
# NOTE(!): to prevent security group resource fail when changing in-place add lifecycle and change from
# using `name` to `name_prefix`, see:
#   * https://github.com/hashicorp/terraform/issues/8617
resource "aws_security_group" "ec2" {
  # Provides a security group resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("asg-%s-ec2-sg-", local.asg_name_prefix_wo_dash)
  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  description = "The Security Group for EC2 Instances launched by LaunchTemplate within the AutoScaling [${local.asg_name_prefix}] Group."
  # (Optional, Forces new resource) The VPC ID.
  vpc_id = var.ecs_asg_vpc_id
  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  tags = merge(
    local.common_tags,
    {
      Name = format("asg-%s-ec2-sg", local.asg_name_prefix_wo_dash)
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_security_group_rule" "ec2_allow_all_egress" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "egress"
  # (Optional) Description of the rule.
  description = "Allow ALL Egress traffic from the EC2 instances."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ec2.*.id)
}
resource "aws_security_group_rule" "ec2_allow_ingress_ports" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? length(var.ecs_asg_open_ports_list) : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow ingress traffic to the EC2 Instances in AutoScaling [${local.asg_name_prefix}] Group on [${var.ecs_asg_open_ports_list[count.index].port}] port from ANY IP."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = var.ecs_asg_open_ports_list[count.index].port
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = var.ecs_asg_open_ports_list[count.index].port
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = var.ecs_asg_open_ports_list[count.index].protocol
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ec2.*.id)
}
resource "aws_security_group_rule" "ec2_allow_ingress_cidrs" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && length(var.ecs_asg_allowed_cidr_blocks_list) > 0) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow ALL the ingress traffic to the EC2 Instances in AutoScaling [${local.asg_name_prefix}] Group from the specified CIDR blocks."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 65535
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = var.ecs_asg_allowed_cidr_blocks_list
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ec2.*.id)
}


# ----------------------------------------------
# Find the latest AMI optimized to use with ECS
# ----------------------------------------------
# NOTE: If more or less than a single match is returned by the search, Terraform will fail. Ensure that your search is
# specific enough to return a single AMI ID only, or use `most_recent` to choose the most recent one. If you want to
# match multiple AMIs, use the `aws_ami_ids` data source instead.
data "aws_ami" "amazon_ecs" {
  # Use this data source to get the ID of a registered AMI for use in other resources.
  count = local.enabled ? 1 : 0

  # (Required) List of AMI owners to limit search. At least 1 value must be specified. Valid values:
  #   * an AWS account ID
  #   * self (the current account)
  #   * an AWS owner alias (e.g. amazon, aws-marketplace, microsoft).
  owners = ["amazon"]

  # (Optional) If more than one result is returned, use the most recent AMI.
  most_recent = true

  # (Optional) One or more name/value pairs to filter off of. There are several valid keys, for a full reference,
  # check out describe-images in the AWS CLI reference:
  #   * http://docs.aws.amazon.com/cli/latest/reference/ec2/describe-images.html
  filter {
    # The name of the AMI (provided during image creation).
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-2*-x86_64-ebs"]
  }
  filter {
    # The virtualization type. Possible values: `paravirtual` or `hvm`.
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    # The image architecture. Possible values: `i386`, `x86_64`, `arm64`.
    name   = "architecture"
    values = ["x86_64"]
  }
}

# ---------------------------------------------------------------------------
# Call the EC2 AutoScaling Group module to create all the required resources
# ---------------------------------------------------------------------------
locals {
  asg_image_id                  = length(var.ecs_asg_image_id) > 0 ? var.ecs_asg_image_id : join("", data.aws_ami.amazon_ecs.*.id)
  asg_iam_instance_profile_name = length(var.ecs_asg_iam_instance_profile_name) > 0 ? var.ecs_asg_iam_instance_profile_name : join("", aws_iam_instance_profile.ec2.*.name)

  asg_security_group_ids = compact(concat(
    var.ecs_asg_security_group_ids,
    [
      join("", aws_security_group.ec2.*.id),
    ],
  ))
}
module "ecs_asg" {
  source = "git::https://gitlab.com/codeassist/terraform/modules/aws-ec2-autoscaling-group.git?ref=tags/2.3.3"

  # --------------------------
  # Common/General parameters
  # --------------------------
  # Whether to create the resources (`false` prevents the module from creating any resources).
  ec2_asg_module_enabled = local.enabled
  # Emulation of `depends_on` behavior for the module (non zero length string can be used to have current module
  # wait for the specified resource.)
  ec2_asg_module_depends_on = [
    aws_iam_role.ec2_instance,
    aws_iam_role_policy_attachment.ecs_ec2,
    aws_security_group.ec2,
    var.ecs_asg_module_depends_on,
  ]
  # (Required) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  ec2_asg_name_prefix = local.asg_name_prefix
  # A mapping of common tags to assign to the resources.
  ec2_asg_tags = local.common_tags
  # A mapping of tags to assign only to EC2 instances.
  ec2_asg_instance_tags = var.ecs_asg_instance_tags
  # A mapping of tags to assign only to EBS volumes.
  ec2_asg_volume_tags = var.ecs_asg_volume_tags

  # -------------------------
  # Launch Template settings
  # -------------------------
  # Specify volumes to attach to the instance besides the volumes specified by the AMI.
  # To find more info, look at:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/block-device-mapping-concepts.html
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#block-devices
  ec2_asg_block_device_mappings = var.ecs_asg_block_device_mappings
  # Customize the credit specification of the instances. See:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#credit-specification
  ec2_asg_credit_specification = var.ecs_asg_credit_specification
  # If `true`, enables EC2 Instance Termination Protection. See:
  #   * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/terminating-instances.html#Using_ChangingDisableAPITermination
  ec2_asg_disable_api_termination = var.ecs_asg_disable_api_termination
  # If true, the launched EC2 instance will be EBS-optimized. (ECS images is always EBS-optimized)
  ec2_asg_ebs_optimized = true
  # The elastic GPU to attach to the instance.
  # See specification of Elastic GPU for more details at:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#elastic-gpu
  ec2_asg_elastic_gpu_specifications = var.ecs_asg_elastic_gpu_specifications
  # The AMI from which to launch the instance.
  ec2_asg_image_id = local.asg_image_id
  # Shutdown behavior for the instance. Can be:
  #   * stop
  #   * terminate
  ec2_asg_instance_initiated_shutdown_behavior = var.ecs_asg_instance_initiated_shutdown_behavior
  # The market (purchasing) option for the instances. See for details:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#market-options
  ec2_asg_instance_market_options = var.ecs_asg_instance_market_options
  # (Required) The type of the instance.
  ec2_asg_instance_type = var.ecs_asg_instance_type
  # The SSH key name that should be used for the instance.
  ec2_asg_key_name = var.ecs_asg_key_name
  # The placement specifications of the instances. See specification of Placement block at:
  #   * https://www.terraform.io/docs/providers/aws/r/launch_template.html#placement-1
  ec2_asg_placement = var.ecs_asg_placement
  # The Base64-encoded user data to provide when launching the instances.
  ec2_asg_user_data = var.ecs_asg_user_data
  # The IAM instance profile name to associate with launched instances.
  ec2_asg_iam_instance_profile_name = local.asg_iam_instance_profile_name
  # Enable/disable detailed monitoring.
  ec2_asg_enable_detailed_monitoring = var.ecs_asg_enable_detailed_monitoring
  # Associate a public IP address with an instance in a VPC.
  ec2_asg_associate_public_ip_address = var.ecs_asg_associate_public_ip_address
  # A list of associated security group IDs.
  ec2_asg_security_group_ids = local.asg_security_group_ids
  # ---------------------------
  # AutoScaling Group settings
  # ---------------------------
  # Whether to replace EC2 instances and existing ASG with new version if any of parameters changes.
  ec2_asg_force_replacement = var.ecs_asg_force_replacement
  # (Required) The maximum size of the autoscale group.
  ec2_asg_max_size = var.ecs_asg_max_size
  # (Required) The minimum size of the autoscale group.
  ec2_asg_min_size = var.ecs_asg_min_size
  # The amount of time, in seconds, after a scaling activity completes before another scaling activity can start.
  ec2_asg_default_cooldown = var.ecs_asg_default_cooldown
  # Time (in seconds) after instance comes into service before checking health.
  ec2_asg_health_check_grace_period = var.ecs_asg_health_check_grace_period
  # Controls how health checking is done. Valid values are `EC2` or `ELB`.
  ec2_asg_health_check_type = var.ecs_asg_health_check_type
  # Allows deleting the autoscaling group without waiting for all instances in the pool to terminate.
  # You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally,
  # Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially
  # leaves resources dangling
  ec2_asg_force_delete = var.ecs_asg_force_delete
  # A list of elastic load balancer names to add to the autoscaling group names.
  # NOTE(!): only valid for classic load balancers. For ALBs, use `target_group_arns` instead
  ec2_asg_load_balancers = var.ecs_asg_load_balancers
  # A list of `aws_alb_target_group` ARNs, for use with Application Load Balancing.
  ec2_asg_target_group_arns = var.ecs_asg_target_group_arns
  # (Required) A list of subnet IDs to launch resources in.
  ec2_asg_subnet_ids = var.ecs_asg_subnet_ids
  # A list of policies to decide how the instances in the auto scale group should be terminated.
  ec2_asg_termination_policies = var.ecs_asg_termination_policies
  # A list of processes to suspend for the AutoScaling Group.
  ec2_asg_suspended_processes = var.ecs_asg_suspended_processes
  # The name of the placement group into which you'll launch your instances, if any.
  ec2_asg_placement_group = var.ecs_asg_placement_group
  # A list of metrics to collect.
  ec2_asg_enabled_metrics = var.ecs_asg_enabled_metrics
  # A maximum duration that Terraform should wait for ASG instances to be healthy before timing out.
  # NOTE(!): setting this to '0' causes Terraform to skip all Capacity Waiting behavior
  ec2_asg_wait_for_capacity_timeout = var.ecs_asg_wait_for_capacity_timeout
  # Setting this causes Terraform to wait for this number of instances to show up healthy in the ELB only on creation.
  # Updates will not wait on ELB instance number changes.
  ec2_asg_min_elb_capacity = var.ecs_asg_min_elb_capacity
  # Setting this will cause Terraform to wait for exactly this number of healthy instances in all attached load
  # balancers on both create and update operations. Takes precedence over `min_elb_capacity` behavior!
  ec2_asg_wait_for_elb_capacity = var.ecs_asg_wait_for_elb_capacity
  # Allows setting instance protection. The autoscaling group will not select instances with this setting for
  # terminination during scale in events.
  ec2_asg_protect_from_scale_in = var.ecs_asg_protect_from_scale_in
  # The ARN of the service-linked role that the ASG will use to call other AWS services.
  ec2_asg_service_linked_role_arn = var.ecs_asg_service_linked_role_arn
  # ---------------------
  # Autoscaling policies
  # ---------------------
  # Whether to create `aws_autoscaling_policy` and `aws_cloudwatch_metric_alarm` resources to control Auto Scaling.
  ec2_asg_autoscaling_policies_enabled = var.ecs_asg_autoscaling_policies_enabled
  ### UP ###
  # Specifies whether the adjustment is an absolute number or a percentage of the current capacity. Valid values are:
  #   * ChangeInCapacity
  #   * ExactCapacity
  #   * PercentChangeInCapacity
  ec2_asg_scale_up_adjustment_type = var.ecs_asg_scale_up_adjustment_type
  # The scalling policy type, either 'SimpleScaling', 'StepScaling' or 'TargetTrackingScaling'.
  ec2_asg_scale_up_policy_type = var.ecs_asg_scale_up_policy_type
  # The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start.
  ec2_asg_scale_up_cooldown_seconds = var.ecs_asg_scale_up_cooldown_seconds
  # The number of instances by which to scale. Adjustment type determines the interpretation of this number
  # (e.g. as an absolute number or as a percentage of the existing Auto Scaling group size). A positive increment
  # adds to the current capacity and a negative value removes from the current capacity
  ec2_asg_scale_up_scaling_adjustment = var.ecs_asg_scale_up_scaling_adjustment
  ### DOWN ###
  # Specifies whether the adjustment is an absolute number or a percentage of the current capacity. Valid values are:
  #   * ChangeInCapacity
  #   * ExactCapacity
  #   * PercentChangeInCapacity
  ec2_asg_scale_down_adjustment_type = var.ecs_asg_scale_down_adjustment_type
  # The scalling policy type, either 'SimpleScaling', 'StepScaling' or 'TargetTrackingScaling'.
  ec2_asg_scale_down_policy_type = var.ecs_asg_scale_down_policy_type
  # The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start.
  ec2_asg_scale_down_cooldown_seconds = var.ecs_asg_scale_down_cooldown_seconds
  # The number of instances by which to scale. Adjustment type determines the interpretation of this number
  # (e.g. as an absolute number or as a percentage of the existing Auto Scaling group size). A positive increment
  # adds to the current capacity and a negative value removes from the current capacity
  ec2_asg_scale_down_scaling_adjustment = var.ecs_asg_scale_down_scaling_adjustment
  # ----------------------------------
  # CloudWatch Metric Alarms settings
  # ----------------------------------
  ### HIGH ###
  # The number of periods over which data is compared to the specified threshold.
  ec2_asg_cpu_utilization_high_evaluation_periods = var.ecs_asg_cpu_utilization_high_evaluation_periods
  # The period in seconds over which the specified statistic is applied.
  ec2_asg_cpu_utilization_high_period_seconds = var.ecs_asg_cpu_utilization_high_period_seconds
  # The statistic to apply to the alarm's associated metric. Either of the following is supported:
  #   * SampleCount
  #   * Average
  #   * Sum
  #   * Minimum
  #   * Maximum
  ec2_asg_cpu_utilization_high_statistic = var.ecs_asg_cpu_utilization_high_statistic
  # The value against which the specified statistic is compared.
  ec2_asg_cpu_utilization_high_threshold_percent = var.ecs_asg_cpu_utilization_high_threshold_percent
  ### LOW ###
  # The number of periods over which data is compared to the specified threshold.
  ec2_asg_cpu_utilization_low_evaluation_periods = var.ecs_asg_cpu_utilization_low_evaluation_periods
  # The period in seconds over which the specified statistic is applied.
  ec2_asg_cpu_utilization_low_period_seconds = var.ecs_asg_cpu_utilization_low_period_seconds
  # The statistic to apply to the alarm's associated metric. Either of the following is supported:
  #   * SampleCount
  #   * Average
  #   * Sum
  #   * Minimum
  #   * Maximum
  ec2_asg_cpu_utilization_low_statistic = var.ecs_asg_cpu_utilization_low_statistic
  # The value against which the specified statistic is compared.
  ec2_asg_cpu_utilization_low_threshold_percent = var.ecs_asg_cpu_utilization_low_threshold_percent
}
